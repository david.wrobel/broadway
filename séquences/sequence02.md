
# Deuxième séquence

Dans cette séquence, on se propose d'apprendre à modulariser.

## Séance 01 

Les objectifs de cette séance sont :

* De découvrir un module existant grâce à sa documentation
* Compléter certaines fonctions de ce module pour valider certains tests
* Intégrer une fonction au module et écrire sa documentation

## Séance 02

Les objectifs de cette séance sont :

* Repérer les attributs et les méthodes propres à un immeuble
* Écrire la classe correspondante
* Écrire sa documentation

## Séance 03

Les objectifs de cette séance sont :

* Utiliser une FILE pour gérer la position des immeubles dans une rue
* Implémenter certaines primitives
* Représenter une rue sur quelques rues données
