
# Première séquence

Dans cette séquence, on se propose d'apprendre à créer des formes simples grâce au module **Turtle**.

## Séance 01 

Les objectifs de cette séance sont :

* Prise en main du module turtle
* Découverte de la gestion du stylo (avancer, reculer, lever, poser)
* Créer des fonctions permettant de tracer des formes simples

## Séance 02

Les objectifs de cette séance sont :

* Ajout de style avec turtle
* Initier à la programmation objet en rassemblant les attributs communs au style (Utilisation des tuples nommés pour gérer les couleurs et les styles)

## Séance 03

Les objectifs de cette séance sont :

* Utiliser du code existant pour tracer un motif
* Utiliser des boucles
* Introduire de l'aléatoire
