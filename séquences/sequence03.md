
# Troisième séquence

Dans cette séquence, on se propose de créer une interface simple pour permettre à un utilisateur lambda de générer sa propre rue.

## Séance 01 

Les objectifs de cette séance sont :

* Découvrir un format de fichier existant (JSON)
* D'utiliser un module RUE pour représenter une rue donnée par un fichier JSON


## Séance 02

Les objectifs de cette séance sont :

* Créer son propre type de fichier à l'aide de balises (*<rue>*, *<immeuble>*, *<style>*)
* Vérifier le bon balisage du fichier à l'aide d'une PILE

## Séance 03

Les objectifs de cette séance sont :

* Compléter le module de gestion du fichier structuré .RUE
* Aller plus loin
