# Broadway

Proposition de séquences pédagogiques pour le projet TraceTaRue !  
Les séances ne se suivent pas forcément !  
C'est un projet que l'on doit réinvestir sur l'année.

## Proposition de structure et positionnement dans la progression

### Structure

On propose de découper ce projet en trois séquences distinctes :

* Première séquence : **Tracer des formes simples** / [Lien](séquences/sequence01.md).
* Deuxième séquence : **Modularité** / [Lien](séquences/sequence02.md)
* Troisème séquence : **Interpréteur** / [Lien](sequence01.md)
